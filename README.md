epub-index
==========
Script to print out all chapter names for an Epub file. Uses `epubzilla` and
`beautifulsoup`.

## License
This code is licensed under the MIT license for Pedro Tacla Yamada, for more
information, please refer to the [LICENSE](/LICENSE) file.

'use strict';
var fs = require('fs');
var request = require('superagent');
var logger = require('superagent-logger');

var token = process.env.TOGGL_API_TOKEN;
var host = 'https://toggl.com';
var pid = 10544393;
var wid = 670027;

var chapN = 0;

function uploadTask(taskName, cb) {
  chapN += 1;
  request
    .post(host + '/api/v8/tasks')
    .auth(token + ':api_token')
    .send({
      task: {
      name: 'Programming Pearls - Chapter ' + chapN + ' - ' + taskName,
      pid: pid,
      wid: wid,
      estimated_seconds: 60 * 60 * 3,
      active: true,
      }
    })
    .use(logger)
    .end(cb);
}

function eachAsync(arr, cb) {
  iter(0);

  function iter(i) {
    if(i >= arr.length) cb();

    var fn = arr[i];
    fn(function(err, res) {
      //if(err) {
        //console.log(res.text)
        //throw err;
      //}
      setTimeout(function() {
        iter(i + 1);
      }, 200)
    });
  }
}

var taskNames = fs.readFileSync('./programming-pearls-chapters').toString().split('\n');
var fns = taskNames.map(function(taskName) {
  return uploadTask.bind(null, taskName);
});

eachAsync(fns, function() {
  console.log('Done');
});

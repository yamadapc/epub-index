'use strict';
var fs = require('fs');
var cheerio = require('cheerio');

var target = fs.readFileSync('./target.html').toString();
var $ = cheerio.load(target);

$('#table-of-contents div').each(function(i, el) {
  var style = $(el).attr('style');
  if(style === 'margin-left: 0.4in;') {
    console.log($(el).text());
  }
}).get();
